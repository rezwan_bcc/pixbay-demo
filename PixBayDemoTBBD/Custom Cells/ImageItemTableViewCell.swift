//
//  ImageItemTableViewCell.swift
//  PixBayDemoTBBD
//
//  Created by Rezwan Islam on 4/12/18.
//  Copyright © 2018 Beyond Innovations & Technologies. All rights reserved.
//

import UIKit

class ImageItemTableViewCell: UITableViewCell {
	@IBOutlet weak var thumbnailImageView: UIImageView!
	@IBOutlet weak var userLabel: UILabel!
	@IBOutlet weak var typeLabel: UILabel!
	@IBOutlet weak var likesLabel: UILabel!
	@IBOutlet weak var imageSizeLabel: UILabel!
	@IBOutlet weak var tagsLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
	
	override func prepareForReuse() {
		thumbnailImageView?.image = nil
	}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
