//
//  UIImageView+Caching.swift
//  PixBayDemoTBBD
//
//  Created by Rezwan Islam on 4/12/18.
//  Copyright © 2018 Beyond Innovations & Technologies. All rights reserved.
//

import UIKit

fileprivate let imageCache = NSCache<NSString,UIImage>()

extension UIImageView {
	func setCachedImage(withURL path:String) {
		let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
		activityIndicator.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
		self.addSubview(activityIndicator)
		self.bringSubviewToFront(activityIndicator)
		activityIndicator.center = self.center
		activityIndicator.startAnimating()
		if let cachedImage = imageCache.object(forKey: NSString(string: path)) {
			self.image = cachedImage
			activityIndicator.stopAnimating()
			activityIndicator.removeFromSuperview()
			return
		}
		if let url = URL(string: path) {
			URLSession.shared.dataTask(with: url) { (data, response, error) in
				if let content = data {
					if let image = UIImage(data: content) {
						DispatchQueue.main.async {
							imageCache.setObject(image, forKey: NSString(string: path))
							self.image = image
							activityIndicator.stopAnimating()
							activityIndicator.removeFromSuperview()
						}
					}
				}
			}.resume()
		}
	}
}
