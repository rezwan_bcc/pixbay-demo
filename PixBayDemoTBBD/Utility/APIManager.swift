//
//  APIManager.swift
//  PixBayDemoTBBD
//
//  Created by Rezwan Islam on 4/12/18.
//  Copyright © 2018 Beyond Innovations & Technologies. All rights reserved.
//

import Foundation

public class APIManager{
	
	let apiKey = "10879422-50a3848f933bb3830b7cf1f57"
	var authorizationToken:String?
	var dataTask:URLSessionDataTask?
	
	static var shared:APIManager = {
		return APIManager()
	}()
	
	func getImages(forSearchText searchText:String, pageNo:Int, pageSize:Int, completion:@escaping (Bool,Error?,[ImageItem]?) -> ()) {
		guard let urlString = "https://pixabay.com/api/?key=\(apiKey)&q=\(searchText)&page=\(pageNo)&per_page=\(pageSize)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
			return
		}
		if let url = URL(string: urlString) {
			dataTask?.cancel()
			dataTask = URLSession.shared.dataTask(with: url) {(data, response, error ) in
				guard error == nil else {
					completion(false,error,nil)
					return
				}
				guard let content = data else {
					completion(false,error,nil)
					return
				}
				if let responseModel = try? JSONDecoder().decode(ResponseModel.self, from: content) {
					completion(true,nil,responseModel.items)
				}
			}
			dataTask?.resume()
		}
	}
}
