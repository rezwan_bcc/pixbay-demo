//
//  ResponseModel.swift
//  PixBayDemoTBBD
//
//  Created by Rezwan Islam on 4/12/18.
//  Copyright © 2018 Beyond Innovations & Technologies. All rights reserved.
//

import Foundation

class ResponseModel : Codable {
	let totalHits : Int?
	let items : [ImageItem]?
	let total : Int?
	
	enum CodingKeys: String, CodingKey {
		case totalHits = "totalHits"
		case items = "hits"
		case total = "total"
	}
	
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		totalHits = try values.decodeIfPresent(Int.self, forKey: .totalHits)
		items = try values.decodeIfPresent([ImageItem].self, forKey: .items)
		total = try values.decodeIfPresent(Int.self, forKey: .total)
	}
}
