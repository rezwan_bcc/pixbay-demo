//
//  ImageItem.swift
//  PixBayDemoTBBD
//
//  Created by Rezwan Islam on 4/12/18.
//  Copyright © 2018 Beyond Innovations & Technologies. All rights reserved.
//

import Foundation
class ImageItem : Codable {
	let largeImageURL : String?
	let webformatHeight : Int?
	let webformatWidth : Int?
	let likes : Int?
	let imageWidth : Int?
	let id : Int?
	let user_id : Int?
	let views : Int?
	let comments : Int?
	let pageURL : String?
	let imageHeight : Int?
	let webformatURL : String?
	let type : String?
	let previewHeight : Int?
	let tags : String?
	let downloads : Int?
	let user : String?
	let favorites : Int?
	let imageSize : Int?
	let previewWidth : Int?
	let userImageURL : String?
	let previewURL : String?
	
	enum CodingKeys: String, CodingKey {
		case largeImageURL = "largeImageURL"
		case webformatHeight = "webformatHeight"
		case webformatWidth = "webformatWidth"
		case likes = "likes"
		case imageWidth = "imageWidth"
		case id = "id"
		case user_id = "user_id"
		case views = "views"
		case comments = "comments"
		case pageURL = "pageURL"
		case imageHeight = "imageHeight"
		case webformatURL = "webformatURL"
		case type = "type"
		case previewHeight = "previewHeight"
		case tags = "tags"
		case downloads = "downloads"
		case user = "user"
		case favorites = "favorites"
		case imageSize = "imageSize"
		case previewWidth = "previewWidth"
		case userImageURL = "userImageURL"
		case previewURL = "previewURL"
	}
	
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		largeImageURL = try values.decodeIfPresent(String.self, forKey: .largeImageURL)
		webformatHeight = try values.decodeIfPresent(Int.self, forKey: .webformatHeight)
		webformatWidth = try values.decodeIfPresent(Int.self, forKey: .webformatWidth)
		likes = try values.decodeIfPresent(Int.self, forKey: .likes)
		imageWidth = try values.decodeIfPresent(Int.self, forKey: .imageWidth)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
		views = try values.decodeIfPresent(Int.self, forKey: .views)
		comments = try values.decodeIfPresent(Int.self, forKey: .comments)
		pageURL = try values.decodeIfPresent(String.self, forKey: .pageURL)
		imageHeight = try values.decodeIfPresent(Int.self, forKey: .imageHeight)
		webformatURL = try values.decodeIfPresent(String.self, forKey: .webformatURL)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		previewHeight = try values.decodeIfPresent(Int.self, forKey: .previewHeight)
		tags = try values.decodeIfPresent(String.self, forKey: .tags)
		downloads = try values.decodeIfPresent(Int.self, forKey: .downloads)
		user = try values.decodeIfPresent(String.self, forKey: .user)
		favorites = try values.decodeIfPresent(Int.self, forKey: .favorites)
		imageSize = try values.decodeIfPresent(Int.self, forKey: .imageSize)
		previewWidth = try values.decodeIfPresent(Int.self, forKey: .previewWidth)
		userImageURL = try values.decodeIfPresent(String.self, forKey: .userImageURL)
		previewURL = try values.decodeIfPresent(String.self, forKey: .previewURL)
	}
}
