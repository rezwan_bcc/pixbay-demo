//
//  ViewController.swift
//  PixBayDemoTBBD
//
//  Created by Rezwan Islam on 4/12/18.
//  Copyright © 2018 Beyond Innovations & Technologies. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	@IBOutlet weak var imageListTableView: UITableView!
	
	let pageSize = 20
	var pageNo = 1
	var imageItemList:[ImageItem] = []
	var isFetching = false
	var searchText = ""
	
	let searchController = UISearchController(searchResultsController: nil)
	let refreshControl = UIRefreshControl()
	var activityIndicator:UIActivityIndicatorView?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		imageListTableView.dataSource = self
		imageListTableView.delegate = self
		activityIndicator = UIActivityIndicatorView(style: .gray)
		activityIndicator?.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
		self.view.addSubview(activityIndicator!)
		self.view.bringSubviewToFront(activityIndicator!)
		activityIndicator?.center = imageListTableView.center
		
		
		searchController.searchResultsUpdater = self
		searchController.delegate = self
		searchController.searchBar.delegate = self
		self.navigationItem.searchController = searchController
		//self.navigationController?.navigationBar.prefersLargeTitles = true
		
		refreshControl.attributedTitle = NSAttributedString(string: "Pull down to refresh")
		refreshControl.addTarget(self, action: #selector(refreshImageList), for: .valueChanged)
		imageListTableView.addSubview(refreshControl)
		fetchImageItems()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.navigationItem.hidesSearchBarWhenScrolling = false
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		self.navigationItem.hidesSearchBarWhenScrolling = true
	}
	
	@objc func refreshImageList() {
		self.pageNo = 1
		searchText = ""
		fetchImageItems()
	}
	
	func fetchImageItems() {
		isFetching = true
		activityIndicator?.startAnimating()
		APIManager.shared.getImages(forSearchText: searchText, pageNo: pageNo, pageSize: pageSize) { (success, error, list) in
			DispatchQueue.main.async {
				self.activityIndicator?.stopAnimating()
				self.refreshControl.endRefreshing()
			}
			if success, let items = list, !items.isEmpty {
				if self.pageNo == 1 {
					self.imageItemList = items
				} else {
					self.imageItemList.append(contentsOf: items)
				}
				self.pageNo += 1
				DispatchQueue.main.async {
					self.imageListTableView.reloadData()
				}
			}
		}
		self.isFetching = false
	}
	
	//MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "ImageDetailSegue" {
			if let detailVC = segue.destination as? DetailViewController,
				let index = sender as? Int {
				detailVC.imageItem = imageItemList[index]
			}
		}
	}

}

extension ViewController:UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return imageItemList.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "ImageItemTableViewCell") as! ImageItemTableViewCell
		let imageItem = imageItemList[indexPath.row]
		if let imagePath = imageItem.previewURL {
			cell.thumbnailImageView?.setCachedImage(withURL: imagePath)
		}
		cell.userLabel.text = "User: \(imageItem.user ?? "")"
		cell.typeLabel.text = "Type: \(imageItem.type ?? "")"
		cell.likesLabel.text = "Total Likes: \(imageItem.likes ?? 0)"
		cell.imageSizeLabel.text = "Resolution: \(imageItem.imageWidth ?? 0) x \(imageItem.imageHeight ?? 0)"
		cell.tagsLabel.text = "Tags: \(imageItem.tags ?? "")"
		return cell
	}
}

extension ViewController:UITableViewDelegate {
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if indexPath.row == imageItemList.count-1 && !isFetching{
			fetchImageItems()
		}
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		performSegue(withIdentifier: "ImageDetailSegue", sender: indexPath.row)
	}
}

extension ViewController:UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate {
	func updateSearchResults(for searchController: UISearchController) {
		if let searchText = searchController.searchBar.text, !searchText.isEmpty {
			pageNo = 1
			self.searchText = searchText
			fetchImageItems()
		}
	}
	
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		searchController.dismiss(animated: true, completion: nil)
		searchController.isActive = false
//		if let searchText = searchBar.text, !searchText.isEmpty {
//			pageNo = 1
//			self.searchText = searchText
//			fetchImageItems()
//		}
	}
}

