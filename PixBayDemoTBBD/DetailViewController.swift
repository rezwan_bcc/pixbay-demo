//
//  DetailViewController.swift
//  PixBayDemoTBBD
//
//  Created by Rezwan Islam on 4/12/18.
//  Copyright © 2018 Beyond Innovations & Technologies. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var userLabel: UILabel!
	@IBOutlet weak var typeLabel: UILabel!
	@IBOutlet weak var likesLabel: UILabel!
	@IBOutlet weak var commentsLabel: UILabel!
	@IBOutlet weak var viewsLabel: UILabel!
	@IBOutlet weak var imageSizeLabel: UILabel!
	@IBOutlet weak var tagsLabel: UILabel!
	
	var imageItem:ImageItem?

    override func viewDidLoad() {
        super.viewDidLoad()
		updateUI()
    }
	
	func updateUI() {
		navigationItem.title = "Image Detail"
		if let imagePath = imageItem?.largeImageURL {
			imageView.setCachedImage(withURL: imagePath)
		}
		userLabel.text = "User: \(imageItem?.user ?? "")"
		typeLabel.text = "Type: \(imageItem?.type ?? "")"
		likesLabel.text = "Total Likes: \(imageItem?.likes ?? 0)"
		commentsLabel.text = "Total Comments: \(imageItem?.comments ?? 0)"
		viewsLabel.text = "Total Views: \(imageItem?.views ?? 0)"
		imageSizeLabel.text = "Resolution: \(imageItem?.imageWidth ?? 0) x \(imageItem?.imageHeight ?? 0)"
		tagsLabel.text = "Tags: \(imageItem?.tags ?? "")"
	}

}
